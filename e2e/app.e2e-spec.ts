import { PughtmlPage } from './app.po';

describe('pughtml App', () => {
  let page: PughtmlPage;

  beforeEach(() => {
    page = new PughtmlPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
