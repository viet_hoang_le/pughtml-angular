import {AfterViewInit, Component} from '@angular/core';
import {ClientSideService} from './services/client-side.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {

  isTopBannerDonateClosed = false;

  constructor(private clientSideService: ClientSideService) {
  }

  ngAfterViewInit(): void {
  }

  onCloseBannerDonate = () => {
    this.isTopBannerDonateClosed = true;
    this.clientSideService.gaEventDonateClose();
  }

  clickDonate = () => this.clientSideService.gaEventDonateClick();
}
