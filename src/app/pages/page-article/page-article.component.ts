import {Component, Input, OnInit} from '@angular/core';
import {IArticle} from '../../interfaces/IArticle';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ArticleService} from '../../services/article.service';
import {Observable} from 'rxjs/Observable';
import {switchMap} from 'rxjs/operators';
import {ClientSideService} from '../../services/client-side.service';

@Component({
  templateUrl: './page-article.component.html',
  styleUrls: ['./page-article.component.scss']
})
export class PageArticleComponent implements OnInit {

  article$: Observable<IArticle>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: ArticleService,
    private clientSideService: ClientSideService,
  ) { }

  ngOnInit() {
    this.article$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.service.getArticle(params.get('id')))
    );

    this.clientSideService.scrollTop();
  }

}
