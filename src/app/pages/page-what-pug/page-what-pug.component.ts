import { Component, OnInit } from '@angular/core';
import {ClientSideService} from '../../services/client-side.service';

@Component({
  templateUrl: './page-what-pug.component.html',
  styleUrls: ['./page-what-pug.component.scss']
})
export class PageWhatPugComponent implements OnInit {

  constructor(private clientSideService: ClientSideService) { }

  ngOnInit() {
    this.clientSideService.scrollTop();
  }

}
