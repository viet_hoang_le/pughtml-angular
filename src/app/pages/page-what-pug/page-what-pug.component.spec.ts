import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageWhatPugComponent } from './page-what-pug.component';

describe('PageWhatPugComponent', () => {
  let component: PageWhatPugComponent;
  let fixture: ComponentFixture<PageWhatPugComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageWhatPugComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageWhatPugComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
