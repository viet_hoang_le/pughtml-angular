import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import * as jsBeautify from 'js-beautify';
import * as html2jade from 'html2jade';
import {ClientSideService} from '../../services/client-side.service';
import {IPugError} from '../../interfaces/IPugError';

const ACE_THEME_DRACULA = 'ace/theme/dracula';
const ACE_THEME_ECLIPSE = 'ace/theme/eclipse';

interface IPugData {
  text: string;
  copytext: string;
  error: IPugError;
  isConverting: boolean;
  theme: string;
}

@Component({
  templateUrl: './page-home.component.html',
  styleUrls: ['./page-home.component.scss']
})
export class PageHomeComponent implements OnInit, AfterViewInit {

  pug: IPugData = {
    text: '',
    copytext: 'copy',
    error: {},
    isConverting: false,
    theme: ACE_THEME_DRACULA,
  };
  html = {
    text: '',
    copytext: 'copy',
    beautified: '',
    isHtmlBeautify: true,
    isJadeBodyless: true,
    isConverting: false,
    theme: ACE_THEME_ECLIPSE,
  };

  private acePugEditor: any;
  private aceHtmlEditor: any;
  private pugTextStream = new Subject<string>();
  private htmlTextStream = new Subject<string>();

  constructor(private clientSideService: ClientSideService) { }

  ngOnInit(): void {
    const sampleHtml = '<div class="card text-center">' +
      '<div class="card-header">Bootstrap 4 card example</div>' +
      '<div class="card-body">' +
      '<div class="card-title">Hello World, this is PugHtml</div>' +
      '<i class="fa fa-home fa-2x text-primary"></i>' +
      '</div>' +
      '<div class="card-footer text-muted">powered by Bibooki</div>' +
      '</div>';

    this.html.text = sampleHtml;
    this.html.beautified = jsBeautify.html_beautify(sampleHtml);
    this.pug.text = '.card.text-center\n' +
      '  .card-header Bootstrap 4 card example\n' +
      '  .card-body\n' +
      '    .card-title Hello World, this is PugHtml\n' +
      '    i.fa.fa-home.fa-2x.text-primary\n' +
      '  .card-footer.text-muted powered by Bibooki';

    if (this.clientSideService.isBrowser) {
      this.subscribePugTextStream();
      this.subscribeHtmlTextStream();
    }

    this.clientSideService.scrollTop();
  }

  ngAfterViewInit(): void {
    this.acePugEditor = this.clientSideService.renderACE('textboxPug', this.pug.theme, this.onPugChanged, this.pug.text);
    this.aceHtmlEditor = this.clientSideService.renderACE('textboxHtml', this.html.theme, this.onHtmlChanged, this.html.beautified);
    this.clientSideService.renderResizable('#draggable', '.left-pug', '.right-html');
  }

  // subscribe the PUG text stream to dispatch after 1000ms delay
  private subscribePugTextStream() {
    this.pugTextStream.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((pugValue: string) => {
        delete this.pug.error;
        return this.triggerConvertPug2Html(pugValue);
      })
    ).subscribe();
  }

  // subscribe the HTML text stream to dispatch after 1000ms delay
  private subscribeHtmlTextStream() {
    this.htmlTextStream.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((htmlValue: string) => {
        delete this.pug.error;
        return this.triggerConvertHtml2Jade(htmlValue);
      })
    ).subscribe();
  }

  triggerConvertPug2Html = async (pugStr: string) => {
    this.pug.isConverting = true;
    this.convertPug2Html(pugStr).then().catch(err => {
      console.error('pug ERROR = ', err);
      this.pug.error = err;
    });
  }

  convertPug2Html = async (pugStr: string) => {
    try {
      delete this.pug.error;
      // pug render options - https://pugjs.org/api/reference.html
      const data = pug.render(pugStr, {pretty: false});
      this.pug.text = pugStr;
      this.html.text = data;
      this.html.beautified = jsBeautify.html_beautify(data);
      this.aceHtmlEditor.setValue(this.html.isHtmlBeautify ? this.html.beautified : this.html.text);
      this.aceHtmlEditor.clearSelection();
      setTimeout(() => this.pug.isConverting = false, 400);
      return pugStr;
    } catch (err) {
      this.pug.isConverting = false;
      throw err;
    }
  }

  triggerConvertHtml2Jade = async (htmlText: string) => {
    this.html.isConverting = true;
    this.convertHtml2Jade(htmlText).then().catch(err => {
      console.error('html ERROR = ', err);
    });
  }

  convertHtml2Jade = async (htmlText: string) => {
    try {
      this.html.text = htmlText;
      this.html.beautified = jsBeautify.html_beautify(htmlText);
      html2jade.convertHtml(htmlText, { bodyless: this.html.isJadeBodyless, noemptypipe: true }, (err, jade) => {
        if (err) {
          console.error('html2jade ERROR = ', err);
          throw err;
        }
        if (jade) {
          this.pug.text = jade;
          this.acePugEditor.setValue(jade);
          this.acePugEditor.clearSelection();
        }
      });
      setTimeout(() => this.html.isConverting = false, 400);
      return htmlText;
    } catch (err) {
      throw err;
    }
  }

  onPugChanged = (text: any) => {
    // checking if the editor is focusing to prevent perform this if the changes triggered from outside
    if (!this.acePugEditor.isFocused()) {
      return;
    }
    if (text !== this.pug.text) {
      this.pug.isConverting = true;
      this.pugTextStream.next(text);
    }
  }

  onHtmlChanged = (text: any) => {
    // checking if the editor is focusing to prevent perform this if the changes triggered from outside
    if (!this.aceHtmlEditor.isFocused()) {
      return;
    }
    if (text !== this.html.text) {
      this.html.isConverting = true;
      this.htmlTextStream.next(text);
    }
  }

  // Changing configurations
  checkJadeBodyless(isChecked: boolean) {
    this.html.isJadeBodyless = isChecked;
    this.triggerConvertHtml2Jade(this.html.text);
  }

  checkHtmlBeautify = (isChecked: boolean) => {
    this.html.isHtmlBeautify = isChecked;
    this.triggerConvertPug2Html(this.pug.text);
  }

  copyPug() {
    if (this.clientSideService.copy2Clipboard(this.pug.text)) {
      this.pug.copytext = 'copied';
      setTimeout(() => this.pug.copytext = 'copy', 1500);
    }
  }

  copyHtml() {
    if (this.clientSideService.copy2Clipboard(this.html.isHtmlBeautify ? this.html.beautified : this.html.text)) {
      this.html.copytext = 'copied';
      setTimeout(() => this.html.copytext = 'copy', 1500);
    }
  }

  clear() {
    this.pug.text = '';
    this.html.text = '';
    this.html.beautified = '';
    this.acePugEditor.setValue('');
    this.aceHtmlEditor.setValue('');
  }

  clickDonate() {
    this.clientSideService.gaEventDonateClick();
  }

  clickTogglePugTheme() {
    this.pug.theme = this.pug.theme === ACE_THEME_DRACULA ? ACE_THEME_ECLIPSE : ACE_THEME_DRACULA;
    this.acePugEditor.setTheme(this.pug.theme);
  }
  clickToggleHtmlTheme() {
    this.html.theme = this.html.theme === ACE_THEME_DRACULA ? ACE_THEME_ECLIPSE : ACE_THEME_DRACULA;
    this.aceHtmlEditor.setTheme(this.html.theme);
  }
}
