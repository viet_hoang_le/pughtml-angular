import { Component, OnInit } from '@angular/core';
import {ClientSideService} from '../../services/client-side.service';

@Component({
  templateUrl: './page-support.component.html',
  styleUrls: ['./page-support.component.scss']
})
export class PageSupportComponent implements OnInit {

  constructor(private clientSideService: ClientSideService) { }

  ngOnInit() {
    this.clientSideService.scrollTop();
  }

}
