import { Component, OnInit } from '@angular/core';
import {IArticle} from '../../interfaces/IArticle';
import {Observable} from 'rxjs/Observable';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ArticleService} from '../../services/article.service';
import {ClientSideService} from '../../services/client-side.service';

@Component({
  templateUrl: './page-how-pug.component.html',
  styleUrls: ['./page-how-pug.component.scss']
})
export class PageHowPugComponent implements OnInit {

  articles$: Observable<IArticle[]>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: ArticleService,
    private clientSideService: ClientSideService
  ) { }

  ngOnInit() {
    this.articles$ = this.service.getArticles();
    this.clientSideService.scrollTop();
  }

}
