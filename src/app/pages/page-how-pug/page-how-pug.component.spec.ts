import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageHowPugComponent } from './page-how-pug.component';

describe('PageHowPugComponent', () => {
  let component: PageHowPugComponent;
  let fixture: ComponentFixture<PageHowPugComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageHowPugComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageHowPugComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
