import {IArticle} from './interfaces/IArticle';

export const ARTICLES: IArticle[] = [
  {
    id: 'pug-angular-cli',
    title: 'Using Pug (Jade) with Angular (with CLI)',
    tags: 'Angular 2, Angular 4, Angular 5',
    description: 'In the tutorial we shown 2 options (pre-compiling & pug-html-loader) to work with pug, to write a cleaner HTML code for Angular.',
    content: '<div class="section-content"><div class="section-inner sectionLayout--outsetColumn"><figure name="8e59" id="8e59" class="graf graf--figure graf--layoutOutsetCenter graf--leading" data-scroll="native"><div class="aspectRatioPlaceholder is-locked" style="max-width: 1000px; max-height: 500px;"><div class="aspectRatioPlaceholder-fill" style="padding-bottom: 50%;"></div><div class="progressiveMedia js-progressiveMedia graf-image is-canvasLoaded is-imageLoaded" data-image-id="1*-mzY9xcuB95RcCtLhXE7kg.png" data-width="1200" data-height="600" data-action="zoom" data-action-value="1*-mzY9xcuB95RcCtLhXE7kg.png" data-scroll="native"><img src="https://cdn-images-1.medium.com/freeze/max/38/1*-mzY9xcuB95RcCtLhXE7kg.png?q=20" crossorigin="anonymous" class="progressiveMedia-thumbnail js-progressiveMedia-thumbnail"><canvas class="progressiveMedia-canvas js-progressiveMedia-canvas" width="75" height="37"></canvas><img class="progressiveMedia-image js-progressiveMedia-image" data-src="https://cdn-images-1.medium.com/max/1250/1*-mzY9xcuB95RcCtLhXE7kg.png" src="https://cdn-images-1.medium.com/max/1250/1*-mzY9xcuB95RcCtLhXE7kg.png"><noscript class="js-progressiveMedia-inner">&lt;img class="progressiveMedia-noscript js-progressiveMedia-inner" src="https://cdn-images-1.medium.com/max/1250/1*-mzY9xcuB95RcCtLhXE7kg.png"&gt;</noscript></div></div></figure></div><div class="section-inner sectionLayout--insetColumn"><h1 name="242e" id="242e" class="graf graf--h3 graf-after--figure graf--title">Using Pug (Jade) with Angular (with&nbsp;CLI)</h1><p name="d64a" id="d64a" class="graf graf--p graf-after--h3">I love <a href="https://pugjs.org/api/getting-started.html" data-href="https://pugjs.org/api/getting-started.html" class="markup--anchor markup--p-anchor" rel="noopener" target="_blank">Pug</a>. Pug allow me to write cleaner HTML.</p><p name="05d4" id="05d4" class="graf graf--p graf-after--p">From HTML like this:-</p><pre name="0e80" id="0e80" class="graf graf--pre graf-after--p">&lt;!DOCTYPE html&gt;</pre><pre name="43d7" id="43d7" class="graf graf--pre graf-after--pre">&lt;html&gt;</pre><pre name="61f7" id="61f7" class="graf graf--pre graf-after--pre">  &lt;head&gt;<br>    &lt;meta charset="utf-8"&gt;<br>    &lt;title&gt;my portal&lt;/title&gt;<br>  &lt;/head&gt;</pre><pre name="e02e" id="e02e" class="graf graf--pre graf-after--pre">  &lt;body&gt;</pre><pre name="fc9d" id="fc9d" class="graf graf--pre graf-after--pre">    &lt;app-root&gt;<br>      &lt;div class="root-spinner"&gt;<br>        &lt;img class="loading" src="assets/images/logo.png"&gt;<br>      &lt;/div&gt;<br>    &lt;/app-root&gt;<br>  &lt;/body&gt;</pre><pre name="4e63" id="4e63" class="graf graf--pre graf-after--pre">&lt;/html&gt;</pre><p name="0659" id="0659" class="graf graf--p graf-after--pre">To Pug, so much cleaner.</p><pre name="7f10" id="7f10" class="graf graf--pre graf-after--p">doctype html</pre><pre name="37db" id="37db" class="graf graf--pre graf-after--pre">html<br>  head<br>    meta(charset="utf-8")<br>    title my portal</pre><pre name="21c0" id="21c0" class="graf graf--pre graf-after--pre">  body<br>    app-root<br>      div.root-spinner<br>      img.loading(src="assets/images/logo.png")</pre><p name="b29c" id="b29c" class="graf graf--p graf-after--pre">However, if you are using Angular (version 2 or 4) &amp; the de-facto Angular-CLI, the pug option is not out of the box, probably yet. There is a discussion here: <a href="https://github.com/angular/angular-cli/issues/1886" data-href="https://github.com/angular/angular-cli/issues/1886" class="markup--anchor markup--p-anchor" rel="nofollow noopener" target="_blank">https://github.com/angular/angular-cli/issues/1886</a>.</p><h3 name="eeb6" id="eeb6" class="graf graf--h3 graf-after--p">How we can use it&nbsp;now?</h3><p name="b75c" id="b75c" class="graf graf--p graf-after--h3">We can use <a href="https://www.npmjs.com/package/pug-cli" data-href="https://www.npmjs.com/package/pug-cli" class="markup--anchor markup--p-anchor" rel="noopener" target="_blank">pug-cli</a> now. Pug CLI allows you to compile pug with command like <code class="markup--code markup--p-code">pug [dir,file]</code>` and watch the changes.</p><pre name="2591" id="2591" class="graf graf--pre graf-after--p">npm install pug-cli --save-dev</pre><p name="d5f7" id="d5f7" class="graf graf--p graf-after--pre">After installation, we create two scripts in <code class="markup--code markup--p-code">package.json</code>`:</p><pre name="6c3e" id="6c3e" class="graf graf--pre graf-after--p">"pug-it": "pug src",<br>"pug-it:w": "pug src -P -w"</pre><p name="e643" id="e643" class="graf graf--p graf-after--pre">The first command `pug-it` will compile all the&nbsp;.pug files under `src` directory to&nbsp;.html file in same directory.</p><p name="1a24" id="1a24" class="graf graf--p graf-after--p">The second command did exactly the same thing with additional file watching. We use this command during development to watch the file changes and recompile automatically.</p><p name="80d2" id="80d2" class="graf graf--p graf-after--p">Here are some other scripts we have.</p><pre name="3372" id="3372" class="graf graf--pre graf-after--p">"ng": "ng",<br>"start": "run-p pug-it:w server",<br>"server": "ng serve --port 4210",<br>"prebuild": "yarn run pug-it",<br>"build": "ng build",</pre><p name="f2f9" id="f2f9" class="graf graf--p graf-after--pre">Please notes that:-</p><ol class="postList"><li name="1b34" id="1b34" class="graf graf--li graf-after--p">When running <code class="markup--code markup--li-code">npm start</code>, we will start the dev server and and the pug watch tasks concurrently.</li><li name="9d0e" id="9d0e" class="graf graf--li graf-after--li">Before we run build, we run pug before that (during <code class="markup--code markup--li-code">prebuild`</code>).</li></ol><h4 name="61e3" id="61e3" class="graf graf--h4 graf-after--li">Side notes on concurrent tasks</h4><p name="f965" id="f965" class="graf graf--p graf-after--h4">If you want to run multiple tasks concurrently cross platforms (mac, windows, etc), 3rd party packages like <a href="https://www.npmjs.com/package/concurrently" data-href="https://www.npmjs.com/package/concurrently" class="markup--anchor markup--p-anchor" rel="noopener" target="_blank">concurrently</a> and <a href="https://www.npmjs.com/package/npm-run-all" data-href="https://www.npmjs.com/package/npm-run-all" class="markup--anchor markup--p-anchor" rel="noopener" target="_blank">npm-run-all</a> are helpful. We use <code class="markup--code markup--p-code">npm-run-all</code>. The <code class="markup--code markup--p-code">run-p</code> command is provided by the package. You can install it:</p><pre name="e5b2" id="e5b2" class="graf graf--pre graf-after--p">npm install npm-run-all --save-dev</pre><p name="053b" id="053b" class="graf graf--p graf-after--pre">However, if all your developers are on Linux or Mac, then you don’t need any 3rd party packages, just replace <code class="markup--code markup--p-code">start</code> command with<code class="markup--code markup--p-code">npm run pug-it:w &amp; npm run server</code> will do.</p><h3 name="4fba" id="4fba" class="graf graf--h3 graf-after--p">Git Ignore HTML&nbsp;files</h3><p name="6008" id="6008" class="graf graf--p graf-after--h3">If you are using pug, you might not want to check in the generated HTML files. Exclude them in your git. Add this line to your&nbsp;<code class="markup--code markup--p-code">.gitignore</code> file.</p><pre name="da7f" id="da7f" class="graf graf--pre graf-after--p">#pug<br>/src/**/*.html</pre><p name="9eec" id="9eec" class="graf graf--p graf-after--pre">Please notes that in your component typescript, you still refer to&nbsp;<code class="markup--code markup--p-code">.html</code>, there’s no need to change this.</p><pre name="aca2" id="aca2" class="graf graf--pre graf-after--p">@Component({<br>   selector: \'my-root\',<br>   templateUrl: \'./app.component.html\',<br>   styleUrls: [\'./app.component.scss\']<br>})<br>export class AppComponent {}</pre><p name="e983" id="e983" class="graf graf--p graf-after--pre">Woala, everything up and running.</p><h3 name="e1a9" id="e1a9" class="graf graf--h3 graf-after--p">Trade-offs</h3><p name="b741" id="b741" class="graf graf--p graf-after--h3">There are a few tradeoffs using this approach.</p><h4 name="0804" id="0804" class="graf graf--h4 graf-after--p"><strong class="markup--strong markup--h4-strong">1. No auto watching for new&nbsp;files</strong></h4><p name="7f07" id="7f07" class="graf graf--p graf-after--h4">When adding any new pug file after <code class="markup--code markup--p-code">npm start</code>, the new file is not watched. You need to restart dev server (stop and rerun <code class="markup--code markup--p-code">npm start</code>).</p><h4 name="00f4" id="00f4" class="graf graf--h4 graf-after--p">2. Fail silently when hitting pug syntax&nbsp;error</h4><p name="8358" id="8358" class="graf graf--p graf-after--h4">Since we start pug watch and dev-server concurrently, when there’s pug syntax error happens during development, you will see errors in terminal, but not on screen or browser console. Please beware of this, sometimes you got syntax error without knowing it, and spend hours to debug other area (you know what I mean).</p><h4 name="8838" id="8838" class="graf graf--h4 graf-after--p">3. Need to manually create pug file if you use <code class="markup--code markup--h4-code">ng&nbsp;generate</code></h4><p name="d7cb" id="d7cb" class="graf graf--p graf-after--h4">By default, angular-cli <code class="markup--code markup--p-code">ng generate</code> will generate HTML file for component. You need to rename or delete/create the HTML to pug file.</p><h3 name="892b" id="892b" class="graf graf--h3 graf-after--p">Some captcha when using pug with&nbsp;Angular</h3><p name="cd22" id="cd22" class="graf graf--p graf-after--h3">Let’s be honest, Angular template syntax looks different from normal HTML. When using it with Pug, you need to surround all attribute with single quote.</p><pre name="f0dc" id="f0dc" class="graf graf--pre graf-after--p">my-top-bar(<br>  \'[(ngModel)]\'="myModel"<br>  \'[propA]\'="myPropA"<br>  \'(actionA)\'="handle($event)"<br>)</pre><p name="c387" id="c387" class="graf graf--p graf-after--pre">This will generate the following HTML.</p><pre name="cd25" id="cd25" class="graf graf--pre graf-after--p">&lt;my-top-bar <br>  [(ngModel)]="myModel"<br>  [propA]="myPropA"<br>  (actionA)="handle($event)"&gt;<br>&lt;/my-top-bar&gt;</pre><p name="9fb9" id="9fb9" class="graf graf--p graf-after--pre">If you somehow forgot to put single quote on the attributes, congratulation. No error will be notify in terminal. The HTML generated will be</p><pre name="6351" id="6351" class="graf graf--pre graf-after--p">&lt;my-top-bar&gt;<br>&lt;/my-top-bar&gt;</pre><p name="071d" id="071d" class="graf graf--p graf-after--pre">All fields will be omitted. If you realise about that after an hour of debugging, please don’t cry. Heh.</p><p name="5cd5" id="5cd5" class="graf graf--p graf-after--p">Please note that</p><h3 name="c7eb" id="c7eb" class="graf graf--h3 graf-after--p">Other alternatives</h3><p name="7833" id="7833" class="graf graf--p graf-after--h3">There are some other alternatives.</p><ol class="postList"><li name="a579" id="a579" class="graf graf--li graf-after--p">First one, the simplest one, don’t use pug, use HTML.</li><li name="d2b5" id="d2b5" class="graf graf--li graf-after--li">Run <code class="markup--code markup--li-code">ng eject</code> to eject the angular-cli. You’ll get back all your webpack config that angular-cli generated for you. Then add your pug-loader.</li><li name="0337" id="0337" class="graf graf--li graf-after--li">Use other angular boilerplate that you can make changes easier. E.g. <a href="https://mgechev.github.io/angular-seed/" data-href="https://mgechev.github.io/angular-seed/" class="markup--anchor markup--li-anchor" rel="noopener" target="_blank">Angular Seed</a>.</li><li name="72f1" id="72f1" class="graf graf--li graf-after--li">Create a pug plugin or angular-cli or wait for one.</li><li name="ba48" id="ba48" class="graf graf--li graf-after--li">Read the discussion <a href="https://github.com/angular/angular-cli/issues/1886" data-href="https://github.com/angular/angular-cli/issues/1886" class="markup--anchor markup--li-anchor" rel="nofollow noopener noopener" target="_blank">https://github.com/angular/angular-cli/issues/1886</a>, probably you can dig out some gold there.</li></ol><h3 name="7290" id="7290" class="graf graf--h3 graf-after--li">Conclusion</h3><p name="5074" id="5074" class="graf graf--p graf-after--h3">Our team found the option we chose and the tradeoffs bearable. We’ve use this in our v2 project now upgrade to v4.</p><p name="6bbf" id="6bbf" class="graf graf--p graf-after--p">Let me know if you have better way of doing this.</p><p name="92d9" id="92d9" class="graf graf--p graf-after--p">That’s it. Hopefully you find this helpful. Happy coding!</p></div></div>',
    url: '//hackernoon.com/using-pug-jade-with-angular-with-cli-5592b7ee24e6',
    imageUrl: '//cdn-images-1.medium.com/max/2000/1*-mzY9xcuB95RcCtLhXE7kg.png',
    createdAt: 1519815397000,
  },
  {
    id: 'pug-sass-scss-vuejs',
    title: 'How to use Pug & Sass (SCSS) in Vue.js',
    tags: 'Pug, sass, scss, Vue.js 2',
    description: '“How can I use my favorite template engine & CSS preprocessor”. First of all, It’s a very simple. You just need to write a few commands and you can use these technologies in your project.',
    content: '<div class="section-content"><div class="section-inner sectionLayout--insetColumn"><h1 name="cdfe" id="cdfe" class="graf graf--h3 graf--leading graf--title">How to use Pug &amp; Sass (SCSS) in Vue.js&nbsp;2?</h1></div><div class="section-inner sectionLayout--outsetColumn"><figure name="345d" id="345d" class="graf graf--figure graf--layoutOutsetCenter graf-after--h3" data-scroll="native"><div class="aspectRatioPlaceholder is-locked" style="max-width: 1000px; max-height: 563px;"><div class="aspectRatioPlaceholder-fill" style="padding-bottom: 56.3%;"></div><div class="progressiveMedia js-progressiveMedia graf-image is-imageLoaded is-canvasLoaded" data-image-id="1*9hECwWZYOruVNR0_WMiJ1w.jpeg" data-width="1920" data-height="1080" data-action="zoom" data-action-value="1*9hECwWZYOruVNR0_WMiJ1w.jpeg" data-scroll="native"><img src="https://cdn-images-1.medium.com/freeze/max/38/1*9hECwWZYOruVNR0_WMiJ1w.jpeg?q=20" crossorigin="anonymous" class="progressiveMedia-thumbnail js-progressiveMedia-thumbnail"><canvas class="progressiveMedia-canvas js-progressiveMedia-canvas" width="75" height="41"></canvas><img class="progressiveMedia-image js-progressiveMedia-image" data-src="https://cdn-images-1.medium.com/max/1250/1*9hECwWZYOruVNR0_WMiJ1w.jpeg" src="https://cdn-images-1.medium.com/max/1250/1*9hECwWZYOruVNR0_WMiJ1w.jpeg"><noscript class="js-progressiveMedia-inner">&lt;img class="progressiveMedia-noscript js-progressiveMedia-inner" src="https://cdn-images-1.medium.com/max/1250/1*9hECwWZYOruVNR0_WMiJ1w.jpeg"&gt;</noscript></div></div></figure></div><div class="section-inner sectionLayout--insetColumn"><p name="17fc" id="17fc" class="graf graf--p graf-after--figure"><em class="markup--em markup--p-em">I think any developer who starts using Vue.js thought about that “How can I use my favorite template engine &amp; CSS preprocessor”. First of all, It’s a very simple. You just need to write a few commands and you can use these technologies in your project. I made this tutorial for a full beginner of a Vue.js framework. Hope you find it useful.</em></p><p name="51f0" id="51f0" class="graf graf--p graf-after--p"><strong class="markup--strong markup--p-strong">So let’s start with a Pug, earlier it is known as a Jade.</strong></p><p name="1fa5" id="1fa5" class="graf graf--p graf-after--p"><em class="markup--em markup--p-em">For start using </em><strong class="markup--strong markup--p-strong"><em class="markup--em markup--p-em">Pug</em></strong><em class="markup--em markup--p-em"> you just need to make a simple command, it’s depends on your package manager.</em></p><p name="a791" id="a791" class="graf graf--p graf-after--p">So, if you using <strong class="markup--strong markup--p-strong">yarn</strong>, type command:</p><pre name="da1d" id="da1d" class="graf graf--pre graf-after--p">yarn add pug pug-loader</pre><p name="268c" id="268c" class="graf graf--p graf-after--pre">If you using <strong class="markup--strong markup--p-strong">NPM</strong>:</p><pre name="b234" id="b234" class="graf graf--pre graf-after--p">npm install pug pug-loader --save-dev</pre><p name="1b6a" id="1b6a" class="graf graf--p graf-after--pre">Now you can use <strong class="markup--strong markup--p-strong"><em class="markup--em markup--p-em">Pug</em></strong> in your project, type <strong class="markup--strong markup--p-strong"><em class="markup--em markup--p-em">lang=”pug” </em></strong>in your project template, like this:</p><pre name="cc1e" id="cc1e" class="graf graf--pre graf-after--p">&lt;<strong class="markup--strong markup--pre-strong">template </strong>lang=<strong class="markup--strong markup--pre-strong">"pug"</strong>&gt;<br>  <br>&lt;/<strong class="markup--strong markup--pre-strong">template</strong>&gt;</pre><p name="5b97" id="5b97" class="graf graf--p graf-after--pre"><strong class="markup--strong markup--p-strong">Okay, you did the first part, let’s move to Sass (SCSS) installations.</strong></p><p name="be61" id="be61" class="graf graf--p graf-after--p">The first thing we need to do is set some dependencies, execute the command below:</p><p name="40be" id="40be" class="graf graf--p graf-after--p">if you use <strong class="markup--strong markup--p-strong">yarn</strong>:</p><pre name="1bed" id="1bed" class="graf graf--pre graf-after--p">yarn add sass-loader node-sass style-loader</pre><p name="6cda" id="6cda" class="graf graf--p graf-after--pre">and of course if you use <strong class="markup--strong markup--p-strong">NPM</strong>, type this:</p><pre name="a45b" id="a45b" class="graf graf--pre graf-after--p">npm install sass-loader node-sass style-loader --save-dev</pre><p name="f6bd" id="f6bd" class="graf graf--p graf-after--pre">Next, you need to add the settings listed below into <strong class="markup--strong markup--p-strong">Webpack</strong>, the configuration file located here&nbsp;: “<strong class="markup--strong markup--p-strong">build / webpack.base.conf.js</strong>”</p><p name="6668" id="6668" class="graf graf--p graf-after--p"><strong class="markup--strong markup--p-strong"><em class="markup--em markup--p-em">Important!</em></strong> <em class="markup--em markup--p-em">Our </em><strong class="markup--strong markup--p-strong"><em class="markup--em markup--p-em">SCSS</em></strong><em class="markup--em markup--p-em"> files are located in </em><strong class="markup--strong markup--p-strong"><em class="markup--em markup--p-em">“src / assets / scss”</em></strong><em class="markup--em markup--p-em">.</em></p><pre name="2be6" id="2be6" class="graf graf--pre graf-after--p">resolve: {<br>  extensions: [\'.js\', \'.vue\', \'.json\', <strong class="markup--strong markup--pre-strong">\'scss\'</strong>],<br>  alias: {<br>    \'vue$\': \'vue/dist/vue.esm.js\',<br>    \'@\': resolve(\'src\'),<br>    <strong class="markup--strong markup--pre-strong">styles: resolve(\'src/assets/scss\')</strong><br>  }<br>},</pre><p name="8394" id="8394" class="graf graf--p graf-after--pre">Now you can use <strong class="markup--strong markup--p-strong">SCSS</strong> in your projects, for this you need to add the not tricky option <strong class="markup--strong markup--p-strong">lang = “scss”</strong> to the section with styles, like so:</p><pre name="e5a7" id="e5a7" class="graf graf--pre graf-after--p">&lt;<strong class="markup--strong markup--pre-strong">style </strong>lang=<strong class="markup--strong markup--pre-strong">"scss"</strong>&gt;<br>  <br>&lt;/<strong class="markup--strong markup--pre-strong">style</strong>&gt;</pre><p name="98b2" id="98b2" class="graf graf--p graf-after--pre">For import&nbsp;<strong class="markup--strong markup--p-strong">.scss</strong> files you need to type <strong class="markup--strong markup--p-strong">“<em class="markup--em markup--p-em">@import</em>”</strong>, for example:</p><pre name="1a28" id="1a28" class="graf graf--pre graf-after--p">&lt;<strong class="markup--strong markup--pre-strong">style </strong>lang=<strong class="markup--strong markup--pre-strong">"scss"</strong>&gt;<br>  @import \'../assets/scss/main\';<br>  <br>&lt;/<strong class="markup--strong markup--pre-strong">style</strong>&gt;</pre><p name="038d" id="038d" class="graf graf--p graf-after--pre">Can also be imported using<strong class="markup--strong markup--p-strong"> JavaScript</strong>, for example:</p><pre name="6781" id="6781" class="graf graf--pre graf-after--p"><strong class="markup--strong markup--pre-strong">&lt;script&gt;</strong><br>import scss from \'./assets/scss/main\'</pre><pre name="628f" id="628f" class="graf graf--pre graf-after--pre"><strong class="markup--strong markup--pre-strong">&lt;/script&gt;</strong></pre><p name="7efb" id="7efb" class="graf graf--p graf-after--pre"><em class="markup--em markup--p-em">Thank you for attention!</em></p><p name="4e8b" id="4e8b" class="graf graf--p graf-after--p graf--trailing"><strong class="markup--strong markup--p-strong"><em class="markup--em markup--p-em">I hope you liked this article, and most important if you found it useful.</em></strong></p></div></div>',
    url: '//codeburst.io/how-to-use-pug-sass-scss-in-vue-js-2-5a78300c4444',
    imageUrl: '//cdn-images-1.medium.com/max/1250/1*9hECwWZYOruVNR0_WMiJ1w.jpeg',
    createdAt: 1519815396000,
  },
  {
    id: 'pug-reactjs',
    title: 'How to use Pug in ReactJS',
    tags: 'Pug, ReactJS',
    description: 'A plugin for transpiling pug templates to jsx',
    content: '<article class="markdown-body entry-content" itemprop="text"><h1><a href="#babel-plugin-transform-react-pug" aria-hidden="true" class="anchor" id="user-content-babel-plugin-transform-react-pug"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>babel-plugin-transform-react-pug</h1>\n' +
    '<p>The official means by which you can use pug in your react components, replaces the use of react-jade when moving to pug.</p>\n' +
    '<p>This plugin transforms the pug inside of your react components.</p>\n' +
    '<h2><a href="#installation" aria-hidden="true" class="anchor" id="user-content-installation"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Installation</h2>\n' +
    '<pre><code>npm install babel-plugin-transform-react-pug --save-dev\n' +
    'npm install babel-plugin-transform-react-jsx --save-dev\n' +
    '</code></pre>\n' +
    '<h2><a href="#usage" aria-hidden="true" class="anchor" id="user-content-usage"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Usage</h2>\n' +
    '<p>.babelrc</p>\n' +
    '<div class="highlight highlight-source-js"><pre>{\n' +
    '  <span class="pl-s"><span class="pl-pds">"</span>plugins<span class="pl-pds">"</span></span><span class="pl-k">:</span> [\n' +
    '    <span class="pl-s"><span class="pl-pds">"</span>transform-react-pug<span class="pl-pds">"</span></span>,\n' +
    '    <span class="pl-s"><span class="pl-pds">"</span>transform-react-jsx<span class="pl-pds">"</span></span>\n' +
    '  ]\n' +
    '}</pre></div>\n' +
    '<h3><a href="#eslint-integration" aria-hidden="true" class="anchor" id="user-content-eslint-integration"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Eslint integration</h3>\n' +
    '<p>Install <a href="https://github.com/ezhlobo/eslint-plugin-react-pug">eslint-plugin-react-pug</a> to be compatible with <a href="https://github.com/yannickcr/eslint-plugin-react">eslint-plugin-react</a>.</p>\n' +
    '<h3><a href="#syntax-highlighting-in-atom" aria-hidden="true" class="anchor" id="user-content-syntax-highlighting-in-atom"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Syntax Highlighting in Atom</h3>\n' +
    '<ol>\n' +
    '<li>\n' +
    '<p>Install <a href="https://atom.io/packages/language-babel" rel="nofollow">language-babel</a> and <a href="https://atom.io/packages/language-pug-jade" rel="nofollow">language-pug-jade</a></p>\n' +
    '<p><em>I suggest language-pug-jade because it works better for me. But there are more approaches for building pugjs grammar: <a href="https://atom.io/packages/language-pug" rel="nofollow">language-pug</a> and <a href="https://atom.io/packages/atom-pug" rel="nofollow">atom-pug</a>, and you can try them too.</em></p>\n' +
    '</li>\n' +
    '<li>\n' +
    '<p>Open settings of language-babel in atom</p>\n' +
    '</li>\n' +
    '<li>\n' +
    '<p>Find the field under "JavaScript Tagged Template Literal Grammar Extensions"</p>\n' +
    '</li>\n' +
    '<li>\n' +
    '<p>Enter: <code>pug:source.pug</code></p>\n' +
    '<p>More details: <a href="https://github.com/gandm/language-babel#javascript-tagged-template-literal-grammar-extensions">gandm/language-babel#javascript-tagged-template-literal-grammar-extensions</a></p>\n' +
    '</li>\n' +
    '<li>\n' +
    '<p>Restart the atom</p>\n' +
    '</li>\n' +
    '</ol>\n' +
    '<h2><a href="#examples" aria-hidden="true" class="anchor" id="user-content-examples"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Examples</h2>\n' +
    '<h3><a href="#example-1---basic-example" aria-hidden="true" class="anchor" id="user-content-example-1---basic-example"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Example 1 - Basic Example</h3>\n' +
    '<p>You can now create a react component with your pug inside it.</p>\n' +
    '<div class="highlight highlight-source-js"><pre><span class="pl-k">import</span> <span class="pl-smi">React</span> <span class="pl-k">from</span> <span class="pl-s"><span class="pl-pds">\'</span>react<span class="pl-pds">\'</span></span>;\n' +
    '\n' +
    '<span class="pl-k">class</span> <span class="pl-en">MyComponent</span> <span class="pl-k">extends</span> <span class="pl-e">React</span>.<span class="pl-smi">Component</span> {\n' +
    '\n' +
    '  <span class="pl-en">render</span>() {\n' +
    '    <span class="pl-k">return</span> pug<span class="pl-s"><span class="pl-pds">`</span></span>\n' +
    '<span class="pl-s">      div</span>\n' +
    '<span class="pl-s">        h1 My Component</span>\n' +
    '<span class="pl-s">        p This is my component using pug.</span>\n' +
    '<span class="pl-s">    <span class="pl-pds">`</span></span>;\n' +
    '  }\n' +
    '}</pre></div>\n' +
    '<h3><a href="#example-2---re-using-a-pug-component" aria-hidden="true" class="anchor" id="user-content-example-2---re-using-a-pug-component"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Example 2 - Re-using a Pug Component</h3>\n' +
    '<p>You can use a pug component in another component.</p>\n' +
    '<div class="highlight highlight-source-js"><pre><span class="pl-k">import</span> <span class="pl-smi">React</span> <span class="pl-k">from</span> <span class="pl-s"><span class="pl-pds">\'</span>react<span class="pl-pds">\'</span></span>;\n' +
    '<span class="pl-k">import</span> <span class="pl-smi">MyComponent</span> <span class="pl-k">from</span> <span class="pl-s"><span class="pl-pds">\'</span>./my-component<span class="pl-pds">\'</span></span>\n' +
    '\n' +
    '<span class="pl-k">class</span> <span class="pl-en">MyNewComponent</span> <span class="pl-k">extends</span> <span class="pl-e">React</span>.<span class="pl-smi">Component</span> {\n' +
    '\n' +
    '  <span class="pl-en">render</span>() {\n' +
    '\n' +
    '    <span class="pl-k">const</span> <span class="pl-c1">prop1</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">\'</span>This is something to pass to another component<span class="pl-pds">\'</span></span>;\n' +
    '\n' +
    '    <span class="pl-k">return</span> pug<span class="pl-s"><span class="pl-pds">`</span></span>\n' +
    '<span class="pl-s">      div</span>\n' +
    '<span class="pl-s">        h1 MyNewComponent</span>\n' +
    '<span class="pl-s">        p This component imports my other component.</span>\n' +
    '<span class="pl-s">        p It could import several of these within the pug.</span>\n' +
    '<span class="pl-s">        MyComponent</span>\n' +
    '<span class="pl-s"></span>\n' +
    '<span class="pl-s">        p If I had created a component with props I could pass them from this component.</span>\n' +
    '<span class="pl-s">        AComponentExpectingProps(</span>\n' +
    '<span class="pl-s">          prop1 = prop1</span>\n' +
    '<span class="pl-s">        )</span>\n' +
    '<span class="pl-s">    <span class="pl-pds">`</span></span>\n' +
    '  }\n' +
    '}</pre></div>\n' +
    '<h3><a href="#example-3---creating-a-pug-constant" aria-hidden="true" class="anchor" id="user-content-example-3---creating-a-pug-constant"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Example 3 - Creating a Pug Constant</h3>\n' +
    '<p>You can create a pug constant that you can simply re-use in your code.</p>\n' +
    '<div class="highlight highlight-source-js"><pre><span class="pl-k">import</span> <span class="pl-smi">React</span> <span class="pl-k">from</span> <span class="pl-s"><span class="pl-pds">\'</span>react<span class="pl-pds">\'</span></span>;\n' +
    '\n' +
    '<span class="pl-k">class</span> <span class="pl-en">MyComponent</span> <span class="pl-k">extends</span> <span class="pl-e">React</span>.<span class="pl-smi">Component</span> {\n' +
    '\n' +
    '  <span class="pl-en">_onDoOneThing</span> <span class="pl-k">=</span> () <span class="pl-k">=&gt;</span> {\n' +
    '    <span class="pl-en">console</span>.<span class="pl-c1">dir</span>(<span class="pl-s"><span class="pl-pds">\'</span>Do one thing<span class="pl-pds">\'</span></span>);\n' +
    '  };\n' +
    '\n' +
    '  <span class="pl-en">_onDoAnotherThing</span> <span class="pl-k">=</span> () <span class="pl-k">=&gt;</span> {\n' +
    '    <span class="pl-en">console</span>.<span class="pl-c1">dir</span>(<span class="pl-s"><span class="pl-pds">\'</span>Do Another thing<span class="pl-pds">\'</span></span>);\n' +
    '  };\n' +
    '\n' +
    '  <span class="pl-en">render</span>() {\n' +
    '\n' +
    '    <span class="pl-k">const</span> <span class="pl-c1">myButtons</span> <span class="pl-k">=</span> pug<span class="pl-s"><span class="pl-pds">`</span></span>\n' +
    '<span class="pl-s">      div</span>\n' +
    '<span class="pl-s">        button(onClick=this._onDoOneThing) Do One Thing</span>\n' +
    '<span class="pl-s">        = \' \'</span>\n' +
    '<span class="pl-s">        button(onClick=this._onDoAnotherThing) Do Another Thing</span>\n' +
    '<span class="pl-s">    <span class="pl-pds">`</span></span>;\n' +
    '\n' +
    '    <span class="pl-k">return</span> pug<span class="pl-s"><span class="pl-pds">`</span></span>\n' +
    '<span class="pl-s">      div</span>\n' +
    '<span class="pl-s">        h1 MyComponent</span>\n' +
    '<span class="pl-s">        p this component uses buttons from my constant.</span>\n' +
    '<span class="pl-s">        div</span>\n' +
    '<span class="pl-s">          = myButtons</span>\n' +
    '<span class="pl-s">    <span class="pl-pds">`</span></span>\n' +
    '  }\n' +
    '}</pre></div>\n' +
    '<h3><a href="#example-4---using-interpolation-in-your-pug-component" aria-hidden="true" class="anchor" id="user-content-example-4---using-interpolation-in-your-pug-component"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Example 4 - Using interpolation in your Pug Component</h3>\n' +
    '<p>If you\'d prefer to use interpolation, you can! This is possible by using <code>${ }</code> within your template.</p>\n' +
    '<p>This lets you benefit from syntax highlighting and linting!</p>\n' +
    '<div class="highlight highlight-source-js"><pre><span class="pl-k">import</span> <span class="pl-smi">React</span> <span class="pl-k">from</span> <span class="pl-s"><span class="pl-pds">\'</span>react<span class="pl-pds">\'</span></span>;\n' +
    '\n' +
    '<span class="pl-k">const</span> <span class="pl-c1">List</span> <span class="pl-k">=</span> (<span class="pl-smi">props</span>) <span class="pl-k">=&gt;</span> {\n' +
    '  <span class="pl-k">return</span> pug<span class="pl-s"><span class="pl-pds">`</span></span>\n' +
    '<span class="pl-s">    ul.list(className=<span class="pl-s1"><span class="pl-pse">${</span> <span class="pl-smi">props</span>.<span class="pl-smi">modifier</span> <span class="pl-pse">}</span></span>)</span>\n' +
    '<span class="pl-s">      <span class="pl-s1"><span class="pl-pse">${</span> <span class="pl-smi">props</span>.<span class="pl-smi">items</span>.<span class="pl-en">map</span>((<span class="pl-smi">item</span>, <span class="pl-smi">index</span>) <span class="pl-k">=&gt;</span> pug<span class="pl-s"><span class="pl-pds">`</span>li.list__item(key=<span class="pl-s1"><span class="pl-pse">${</span> index <span class="pl-pse">}</span></span>) <span class="pl-s1"><span class="pl-pse">${</span> item <span class="pl-pse">}</span></span><span class="pl-pds">`</span></span> ) <span class="pl-pse">}</span></span></span>\n' +
    '<span class="pl-s">  <span class="pl-pds">`</span></span>\n' +
    '}</pre></div>\n' +
    '</article>',
    url: '//github.com/pugjs/babel-plugin-transform-react-pug',
    imageUrl: '//reactjs.org/logo-og.png',
    createdAt: 1519815395000,
  },
  {
    id: 'pug-php',
    title: 'Pug-php',
    tags: 'Pug, PHP',
    description: 'Pug-php adds inline PHP scripting support to the Pug template compiler. Since the version 3, it uses Phug, a very customizable Pug template engine made by Tale-pug and Pug-php developpers as the new PHP Pug engine reference.',
    content: '<article class="markdown-body entry-content" itemprop="text"><h1><a href="#pug-php" aria-hidden="true" class="anchor" id="user-content-pug-php"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Pug-php</h1>\n' +
    '\n' +
    '\n' +
    '<p><strong>Pug-php</strong> adds inline PHP scripting support to the <a href="https://pugjs.org" rel="nofollow">Pug</a> template compiler. Since the version 3, it uses <strong>Phug</strong>, a very customizable Pug template engine made by <strong>Tale-pug</strong> and <strong>Pug-php</strong> developpers as the new PHP Pug engine reference.</p>\n' +
    '<h5><a href="#official-phug-documentation" aria-hidden="true" class="anchor" id="user-content-official-phug-documentation"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a><a href="https://www.phug-lang.com/" rel="nofollow">Official Phug documentation</a></h5>\n' +
    '<h5><a href="#see-pug-php-demo" aria-hidden="true" class="anchor" id="user-content-see-pug-php-demo"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a><a href="https://pug-demo.herokuapp.com/" rel="nofollow">See Pug-php demo</a></h5>\n' +
    '<h2><a href="#install" aria-hidden="true" class="anchor" id="user-content-install"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Install</h2>\n' +
    '<p>First you need composer if you have\'nt yet: <a href="https://getcomposer.org/download/" rel="nofollow">https://getcomposer.org/download/</a></p>\n' +
    '<p>Then run:</p>\n' +
    '<div class="highlight highlight-source-shell"><pre>composer require pug-php/pug</pre></div>\n' +
    '<h2><a href="#pug-in-your-favorite-framework" aria-hidden="true" class="anchor" id="user-content-pug-in-your-favorite-framework"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Pug in your favorite framework</h2>\n' +
    '<p>Phalcon: <a href="https://github.com/pug-php/pug-phalcon">https://github.com/pug-php/pug-phalcon</a></p>\n' +
    '<p>Symfony: <a href="https://github.com/pug-php/pug-symfony">https://github.com/pug-php/pug-symfony</a></p>\n' +
    '<p>Laravel: <a href="https://github.com/BKWLD/laravel-pug">https://github.com/BKWLD/laravel-pug</a></p>\n' +
    '<p>CodeIgniter: <a href="https://github.com/pug-php/ci-pug-engine">https://github.com/pug-php/ci-pug-engine</a></p>\n' +
    '<p>Yii 2: <a href="https://github.com/rmrevin/yii2-pug">https://github.com/rmrevin/yii2-pug</a></p>\n' +
    '<p>Slim 3: <a href="https://github.com/MarcelloDuarte/pug-slim">https://github.com/MarcelloDuarte/pug-slim</a></p>\n' +
    '<h2><a href="#use" aria-hidden="true" class="anchor" id="user-content-use"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Use</h2>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-pse">&lt;?php</span><span class="pl-s1"></span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1"><span class="pl-k">include</span> <span class="pl-s"><span class="pl-pds">\'</span>vendor/autoload.php<span class="pl-pds">\'</span></span>;</span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$pug</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">Pug</span>([</span>\n' +
    '<span class="pl-s1">    <span class="pl-c"><span class="pl-c">//</span> here you can set options</span></span>\n' +
    '<span class="pl-s1">]);</span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>displayFile(<span class="pl-s"><span class="pl-pds">\'</span>my-pug-template.pug<span class="pl-pds">\'</span></span>);</span></pre></div>\n' +
    '<p>Since <strong>Pug-php</strong> 3.1.2, you no longer need to import the class with\n' +
    '<code>use Pug\\Pug;</code> as we provide an alias.</p>\n' +
    '<p>Main methods are <code>render</code>, <code>renderFile</code>, <code>compile</code>, <code>compileFile</code>,\n' +
    '<code>display</code>, <code>displayFile</code> and <code>setOption</code>, see the complete documentation\n' +
    'here: <a href="https://www.phug-lang.com" rel="nofollow">phug-lang.com</a>.</p>\n' +
    '<p>You can also use the facade to call methods statically:</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-pse">&lt;?php</span><span class="pl-s1"></span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1"><span class="pl-k">use</span> <span class="pl-c1">Pug\\Facade</span> <span class="pl-k">as</span> <span class="pl-c1">PugFacade</span>;</span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1"><span class="pl-k">include</span> <span class="pl-s"><span class="pl-pds">\'</span>vendor/autoload.php<span class="pl-pds">\'</span></span>;</span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$html</span> <span class="pl-k">=</span> <span class="pl-c1">PugFacade</span><span class="pl-k">::</span>renderFile(<span class="pl-s"><span class="pl-pds">\'</span>my-pug-template.pug<span class="pl-pds">\'</span></span>);</span></pre></div>\n' +
    '<h2><a href="#pug-options" aria-hidden="true" class="anchor" id="user-content-pug-options"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Pug options</h2>\n' +
    '<p>Pug options should be passed to the constructor</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-smi">$pug</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">Pug</span>(<span class="pl-c1">array</span>(</span>\n' +
    '<span class="pl-s1">    <span class="pl-s"><span class="pl-pds">\'</span>pretty<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">true</span>,</span>\n' +
    '<span class="pl-s1">    <span class="pl-s"><span class="pl-pds">\'</span>cache<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">\'</span>pathto/writable/cachefolder/<span class="pl-pds">\'</span></span></span>\n' +
    '<span class="pl-s1">));</span></pre></div>\n' +
    '<h2><a href="#supports-for-local-variables" aria-hidden="true" class="anchor" id="user-content-supports-for-local-variables"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Supports for local variables</h2>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-smi">$pug</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">Pug</span>();</span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$output</span> <span class="pl-k">=</span> <span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>render(<span class="pl-s"><span class="pl-pds">\'</span>file<span class="pl-pds">\'</span></span>, <span class="pl-c1">array</span>(</span>\n' +
    '<span class="pl-s1">    <span class="pl-s"><span class="pl-pds">\'</span>title<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">\'</span>Hello World<span class="pl-pds">\'</span></span></span>\n' +
    '<span class="pl-s1">));</span></pre></div>\n' +
    '<h2><a href="#new-in-pug-php-3" aria-hidden="true" class="anchor" id="user-content-new-in-pug-php-3"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>New in pug-php 3</h2>\n' +
    '<p>pug-php 3 is now aligned on <a href="/pug-php/pug/blob/master/github.com/pugjs/pug">pugjs 2</a>, it aims to be a perfect\n' +
    'port of the JS project. That\'s why there are breaking changes in this new version.</p>\n' +
    '<p><a href="https://github.com/pug-php/pug/blob/master/CHANGELOG.md">See the changelog to know what\'s new</a></p>\n' +
    '<p><a href="https://github.com/pug-php/pug/blob/master/MIGRATION_GUIDE.md">See the migration guide if you want to upgrade from pug-php 2 to 3</a></p>\n' +
    '<h2><a href="#supports-for-custom-filters" aria-hidden="true" class="anchor" id="user-content-supports-for-custom-filters"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Supports for custom filters</h2>\n' +
    '<p>Filters must be callable: It can be a class that implements the <em>__invoke()</em> method, or an anonymous function.</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>filter(<span class="pl-s"><span class="pl-pds">\'</span>escaped<span class="pl-pds">\'</span></span>, <span class="pl-s"><span class="pl-pds">\'</span>My\\Callable\\Class<span class="pl-pds">\'</span></span>);</span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1"><span class="pl-c"><span class="pl-c">//</span> or</span></span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>filter(<span class="pl-s"><span class="pl-pds">\'</span>escaped<span class="pl-pds">\'</span></span>, <span class="pl-k">function</span>(<span class="pl-smi">$node</span>, <span class="pl-smi">$compiler</span>){</span>\n' +
    '<span class="pl-s1">    <span class="pl-k">foreach</span> (<span class="pl-smi">$node</span><span class="pl-k">-&gt;</span><span class="pl-smi">block</span><span class="pl-k">-&gt;</span><span class="pl-smi">nodes</span> <span class="pl-k">as</span> <span class="pl-smi">$line</span>) {</span>\n' +
    '<span class="pl-s1">        <span class="pl-smi">$output</span>[] <span class="pl-k">=</span> <span class="pl-smi">$compiler</span><span class="pl-k">-&gt;</span>interpolate(<span class="pl-smi">$line</span><span class="pl-k">-&gt;</span><span class="pl-smi">value</span>);</span>\n' +
    '<span class="pl-s1">    }</span>\n' +
    '<span class="pl-s1">    <span class="pl-k">return</span> <span class="pl-c1">htmlentities</span>(<span class="pl-c1">implode</span>(<span class="pl-s"><span class="pl-pds">"</span><span class="pl-cce">\\n</span><span class="pl-pds">"</span></span>, <span class="pl-smi">$output</span>));</span>\n' +
    '<span class="pl-s1">});</span></pre></div>\n' +
    '<h3><a href="#built-in-filters" aria-hidden="true" class="anchor" id="user-content-built-in-filters"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Built-in filters</h3>\n' +
    '<ul>\n' +
    '<li>:css</li>\n' +
    '<li>:php</li>\n' +
    '<li>:javascript</li>\n' +
    '<li>:escaped</li>\n' +
    '<li>:cdata</li>\n' +
    '</ul>\n' +
    '<h3><a href="#install-other-filters-with-composer" aria-hidden="true" class="anchor" id="user-content-install-other-filters-with-composer"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Install other filters with composer</h3>\n' +
    '<p><a href="http://pug-filters.selfbuild.fr/" rel="nofollow">http://pug-filters.selfbuild.fr/</a></p>\n' +
    '<h3><a href="#publish-your-own-filter" aria-hidden="true" class="anchor" id="user-content-publish-your-own-filter"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Publish your own filter</h3>\n' +
    '<p><a href="https://github.com/pug-php/pug-filter-base#readme">https://github.com/pug-php/pug-filter-base#readme</a></p>\n' +
    '<h2><a href="#supports-for-custom-keywords" aria-hidden="true" class="anchor" id="user-content-supports-for-custom-keywords"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Supports for custom keywords</h2>\n' +
    '<p>You can add custom keywords, here are some examples:</p>\n' +
    '<p><strong>Anonymous function</strong>:</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>addKeyword(<span class="pl-s"><span class="pl-pds">\'</span>range<span class="pl-pds">\'</span></span>, <span class="pl-k">function</span> (<span class="pl-smi">$args</span>) {</span>\n' +
    '<span class="pl-s1">    <span class="pl-c1">list</span>(<span class="pl-smi">$from</span>, <span class="pl-smi">$to</span>) <span class="pl-k">=</span> <span class="pl-c1">explode</span>(<span class="pl-s"><span class="pl-pds">\'</span> <span class="pl-pds">\'</span></span>, <span class="pl-c1">trim</span>(<span class="pl-smi">$args</span>));</span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1">    <span class="pl-k">return</span> <span class="pl-c1">array</span>(</span>\n' +
    '<span class="pl-s1">        <span class="pl-s"><span class="pl-pds">\'</span>beginPhp<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">\'</span>for ($i = <span class="pl-pds">\'</span></span> <span class="pl-k">.</span> <span class="pl-smi">$from</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">\'</span>; $i &lt;= <span class="pl-pds">\'</span></span> <span class="pl-k">.</span> <span class="pl-smi">$to</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">\'</span>; $i++) {<span class="pl-pds">\'</span></span>,</span>\n' +
    '<span class="pl-s1">        <span class="pl-s"><span class="pl-pds">\'</span>endPhp<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">\'</span>}<span class="pl-pds">\'</span></span>,</span>\n' +
    '<span class="pl-s1">    );</span>\n' +
    '<span class="pl-s1">});</span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>render(<span class="pl-s"><span class="pl-pds">\'</span></span></span>\n' +
    '<span class="pl-s1"><span class="pl-s">range 1 3</span></span>\n' +
    '<span class="pl-s1"><span class="pl-s">    p= i</span></span>\n' +
    '<span class="pl-s1"><span class="pl-s"><span class="pl-pds">\'</span></span>);</span></pre></div>\n' +
    '<p>This will render:</p>\n' +
    '<div class="highlight highlight-text-html-basic"><pre>&lt;<span class="pl-ent">p</span>&gt;1&lt;/<span class="pl-ent">p</span>&gt;\n' +
    '&lt;<span class="pl-ent">p</span>&gt;2&lt;/<span class="pl-ent">p</span>&gt;\n' +
    '&lt;<span class="pl-ent">p</span>&gt;3&lt;/<span class="pl-ent">p</span>&gt;</pre></div>\n' +
    '<p>Note that the existing <code>for..in</code> operator will have the precedence on this custom <code>for</code> keyword.</p>\n' +
    '<p><strong>Invokable class</strong>:</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-k">class</span> <span class="pl-en">UserKeyword</span></span>\n' +
    '<span class="pl-s1">{</span>\n' +
    '<span class="pl-s1">    <span class="pl-k">public</span> <span class="pl-k">function</span> <span class="pl-c1">__invoke</span>(<span class="pl-smi">$arguments</span>, <span class="pl-smi">$block</span>, <span class="pl-smi">$keyWord</span>)</span>\n' +
    '<span class="pl-s1">    {</span>\n' +
    '<span class="pl-s1">        <span class="pl-smi">$badges</span> <span class="pl-k">=</span> <span class="pl-c1">array</span>();</span>\n' +
    '<span class="pl-s1">        <span class="pl-k">foreach</span> (<span class="pl-smi">$block</span><span class="pl-k">-&gt;</span><span class="pl-smi">nodes</span> <span class="pl-k">as</span> <span class="pl-smi">$index</span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$tag</span>) {</span>\n' +
    '<span class="pl-s1">            <span class="pl-k">if</span> (<span class="pl-smi">$tag</span><span class="pl-k">-&gt;</span><span class="pl-smi">name</span> <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">\'</span>badge<span class="pl-pds">\'</span></span>) {</span>\n' +
    '<span class="pl-s1">                <span class="pl-smi">$href</span> <span class="pl-k">=</span> <span class="pl-smi">$tag</span><span class="pl-k">-&gt;</span>getAttribute(<span class="pl-s"><span class="pl-pds">\'</span>color<span class="pl-pds">\'</span></span>);</span>\n' +
    '<span class="pl-s1">                <span class="pl-smi">$badges</span>[] <span class="pl-k">=</span> <span class="pl-smi">$href</span>[<span class="pl-s"><span class="pl-pds">\'</span>value<span class="pl-pds">\'</span></span>];</span>\n' +
    '<span class="pl-s1">                <span class="pl-c1">unset</span>(<span class="pl-smi">$block</span><span class="pl-k">-&gt;</span><span class="pl-smi">nodes</span>[<span class="pl-smi">$index</span>]);</span>\n' +
    '<span class="pl-s1">            }</span>\n' +
    '<span class="pl-s1">        }</span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1">        <span class="pl-k">return</span> <span class="pl-c1">array</span>(</span>\n' +
    '<span class="pl-s1">            <span class="pl-s"><span class="pl-pds">\'</span>begin<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">\'</span>&lt;div class="<span class="pl-pds">\'</span></span> <span class="pl-k">.</span> <span class="pl-smi">$keyWord</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">\'</span>" data-name="<span class="pl-pds">\'</span></span> <span class="pl-k">.</span> <span class="pl-smi">$arguments</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">\'</span>" data-badges="[<span class="pl-pds">\'</span></span> <span class="pl-k">.</span> <span class="pl-c1">implode</span>(<span class="pl-s"><span class="pl-pds">\'</span>,<span class="pl-pds">\'</span></span>, <span class="pl-smi">$badges</span>) <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">\'</span>]"&gt;<span class="pl-pds">\'</span></span>,</span>\n' +
    '<span class="pl-s1">            <span class="pl-s"><span class="pl-pds">\'</span>end<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">\'</span>&lt;/div&gt;<span class="pl-pds">\'</span></span>,</span>\n' +
    '<span class="pl-s1">        );</span>\n' +
    '<span class="pl-s1">    }</span>\n' +
    '<span class="pl-s1">}</span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>addKeyword(<span class="pl-s"><span class="pl-pds">\'</span>user<span class="pl-pds">\'</span></span>, <span class="pl-k">new</span> <span class="pl-c1">UserKeyword</span>());</span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>render(<span class="pl-s"><span class="pl-pds">\'</span></span></span>\n' +
    '<span class="pl-s1"><span class="pl-s">user Bob</span></span>\n' +
    '<span class="pl-s1"><span class="pl-s">    badge(color="blue")</span></span>\n' +
    '<span class="pl-s1"><span class="pl-s">    badge(color="red")</span></span>\n' +
    '<span class="pl-s1"><span class="pl-s">    em Registered yesterday</span></span>\n' +
    '<span class="pl-s1"><span class="pl-s"><span class="pl-pds">\'</span></span>);</span></pre></div>\n' +
    '<p>This will render:</p>\n' +
    '<div class="highlight highlight-text-html-basic"><pre>&lt;<span class="pl-ent">div</span> <span class="pl-e">class</span>=<span class="pl-s"><span class="pl-pds">"</span>user<span class="pl-pds">"</span></span> <span class="pl-e">data-name</span>=<span class="pl-s"><span class="pl-pds">"</span>Bob<span class="pl-pds">"</span></span> <span class="pl-e">data-badges</span>=<span class="pl-s"><span class="pl-pds">"</span>[\'blue\', \'red\']<span class="pl-pds">"</span></span>&gt;\n' +
    '    &lt;<span class="pl-ent">em</span>&gt;Registered yesterday&lt;/<span class="pl-ent">em</span>&gt;\n' +
    '&lt;/<span class="pl-ent">div</span>&gt;</pre></div>\n' +
    '<p>A keyword must return an array (containing <strong>begin</strong> and/or <strong>end</strong> entires) or a string (used as a <strong>begin</strong> entry).</p>\n' +
    '<p>The <strong>begin</strong> and <strong>end</strong> are rendered as raw HTML, but you can also use <strong>beginPhp</strong> and <strong>endPhp</strong> as in the first example to render PHP codes that will wrap the rendered block if there is one.</p>\n' +
    '<h2><a href="#php-helpers-functions" aria-hidden="true" class="anchor" id="user-content-php-helpers-functions"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>PHP Helpers functions</h2>\n' +
    '<p>If you want to make a php function available in a template or in all of them for convenience, use closures ans pass them like any other variables:</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-smi">$myClosure</span> <span class="pl-k">=</span> <span class="pl-k">function</span> (<span class="pl-smi">$string</span>) {</span>\n' +
    '<span class="pl-s1">    <span class="pl-k">return</span> <span class="pl-s"><span class="pl-pds">\'</span>Hey you <span class="pl-pds">\'</span></span> <span class="pl-k">.</span> <span class="pl-smi">$string</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">\'</span>, out there on your own, can you hear me?<span class="pl-pds">\'</span></span>;</span>\n' +
    '<span class="pl-s1">};</span>\n' +
    '<span class="pl-s1"></span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>render(<span class="pl-s"><span class="pl-pds">\'</span>p=$myClosure("Pink")<span class="pl-pds">\'</span></span>, <span class="pl-c1">array</span>(<span class="pl-s"><span class="pl-pds">\'</span>myClosure<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$myClosure</span>));</span></pre></div>\n' +
    '<p>This will render:</p>\n' +
    '<div class="highlight highlight-text-html-basic"><pre>&lt;<span class="pl-ent">p</span>&gt;Hey you Pink, out there on your own, can you hear me?&lt;/<span class="pl-ent">p</span>&gt;</pre></div>\n' +
    '<p>You can make that closure available to all templates without passing it in render params by using the <code>share</code> method:</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-c"><span class="pl-c">//</span> ... $pug instantiation</span></span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>share(<span class="pl-s"><span class="pl-pds">\'</span>myClosure<span class="pl-pds">\'</span></span>, <span class="pl-smi">$myClosure</span>);</span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>render(<span class="pl-s"><span class="pl-pds">\'</span>p=$myClosure("Pink")<span class="pl-pds">\'</span></span>);</span></pre></div>\n' +
    '<p>This will render the same HTML than the previous example. Also note that <code>share</code> allow you to pass any type of value.</p>\n' +
    '<h2><a href="#cache" aria-hidden="true" class="anchor" id="user-content-cache"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Cache</h2>\n' +
    '<p><strong>Important</strong>: to improve performance in production, enable the Pug cache by setting the <strong>cache</strong> option to a writable directory, you can first cache all your template at once (during deployment):</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-smi">$pug</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">Pug</span>(<span class="pl-c1">array</span>(</span>\n' +
    '<span class="pl-s1">    <span class="pl-s"><span class="pl-pds">\'</span>cache<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">\'</span>var/cache/pug<span class="pl-pds">\'</span></span>,</span>\n' +
    '<span class="pl-s1">);</span>\n' +
    '<span class="pl-s1"><span class="pl-c1">list</span>(<span class="pl-smi">$success</span>, <span class="pl-smi">$errors</span>) <span class="pl-k">=</span> <span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>cacheDirectory(<span class="pl-s"><span class="pl-pds">\'</span>path/to/pug/templates<span class="pl-pds">\'</span></span>);</span>\n' +
    '<span class="pl-s1"><span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$success</span> files have been cached<span class="pl-cce">\\n</span><span class="pl-pds">"</span></span>;</span>\n' +
    '<span class="pl-s1"><span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">"</span><span class="pl-smi">$errors</span> errors occurred<span class="pl-cce">\\n</span><span class="pl-pds">"</span></span>;</span></pre></div>\n' +
    '<p>Be sure any unexpected error occurred and that all your templates in your template directory have been cached.</p>\n' +
    '<p>Then use the same cache directory and template directory in production with the option upToDateCheck to <code>false</code>\n' +
    'to bypass the cache check and automatically use the cache version:</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-smi">$pug</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">Pug</span>(<span class="pl-c1">array</span>(</span>\n' +
    '<span class="pl-s1">    <span class="pl-s"><span class="pl-pds">\'</span>cache<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">\'</span>var/cache/pug<span class="pl-pds">\'</span></span>,</span>\n' +
    '<span class="pl-s1">    <span class="pl-s"><span class="pl-pds">\'</span>basedir<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">\'</span>path/to/pug/templates<span class="pl-pds">\'</span></span>,</span>\n' +
    '<span class="pl-s1">    <span class="pl-s"><span class="pl-pds">\'</span>upToDateCheck<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">false</span>,</span>\n' +
    '<span class="pl-s1">);</span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>render(<span class="pl-s"><span class="pl-pds">\'</span>path/to/pug/templates/my-page.pug<span class="pl-pds">\'</span></span>);</span></pre></div>\n' +
    '<h2><a href="#templates-from-pug-js" aria-hidden="true" class="anchor" id="user-content-templates-from-pug-js"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Templates from pug-js</h2>\n' +
    '<p>First remember pug-php is a PHP template engine. Pug-js and Pug-php provide both a HAML-like syntax\n' +
    'for markup, but for expression and raw code, pug-js use JS, and pug-php use PHP. By default, we did\n' +
    'some magic tricks to transform simple JS syntaxes into PHP. This should help you to migrate smoother\n' +
    'from pug-js if you already have some template but benefit of PHP advantages.</p>\n' +
    '<p>If you start a new project, we highly recommend you to use the following option:</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-smi">$pug</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">Pug</span>(<span class="pl-c1">array</span>(</span>\n' +
    '<span class="pl-s1">    <span class="pl-s"><span class="pl-pds">\'</span>expressionLanguage<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">\'</span>php<span class="pl-pds">\'</span></span></span>\n' +
    '<span class="pl-s1">);</span></pre></div>\n' +
    '<p>It will disable all translations, so you have to use always explicit PHP syntaxes such as:</p>\n' +
    '<div class="highlight highlight-text-jade"><pre><span class="pl-s1">- $concat <span class="pl-k">=</span> <span class="pl-smi">$foo</span> . <span class="pl-smi">$bar</span></span>\n' +
    'p<span class="pl-s1"><span class="pl-c1">=</span>$concat</span></pre></div>\n' +
    '<p>If you want expressions closest to JS, you can use:</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-smi">$pug</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">Pug</span>(<span class="pl-c1">array</span>(</span>\n' +
    '<span class="pl-s1">    <span class="pl-s"><span class="pl-pds">\'</span>expressionLanguage<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">\'</span>js<span class="pl-pds">\'</span></span></span>\n' +
    '<span class="pl-s1">);</span></pre></div>\n' +
    '<p>It will allow both PHP stuff and JS stuff in a JS-style syntax. But you must stick to it,\n' +
    'you will not be able to mix PHP and JS styles in this mode.</p>\n' +
    '<p>Finally, you can use the native pug-js engine with:</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-smi">$pug</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">Pug</span>(<span class="pl-c1">array</span>(</span>\n' +
    '<span class="pl-s1">    <span class="pl-s"><span class="pl-pds">\'</span>pugjs<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">true</span></span>\n' +
    '<span class="pl-s1">);</span></pre></div>\n' +
    '<p>This mode require node and npm installed as it will install <strong>pug-cli</strong> and directly call it.\n' +
    'This mode will flat you local vars (it means complex object like DateTime, or classes with\n' +
    'magic methods will be striginfied in JSON as simple objects) and you will not benefit some\n' +
    'features like mixed indent, pre/post render hooks but in this mode you will get exact same\n' +
    'output as in pug-js.</p>\n' +
    '<h3><a href="#write-locals-object-to-json-file-with-pugjs" aria-hidden="true" class="anchor" id="user-content-write-locals-object-to-json-file-with-pugjs"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Write locals object to json file with pugjs</h3>\n' +
    '<p>If your locals object is large it may cause a <code>RuntimeException</code> error. This is because\n' +
    'locals object passed directly to pug-cli as argument. To fix this problem you can use\n' +
    '<code>localsJsonFile</code> option:</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-smi">$pug</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">Pug</span>(<span class="pl-c1">array</span>(</span>\n' +
    '<span class="pl-s1">    <span class="pl-s"><span class="pl-pds">\'</span>pugjs<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">true</span>,</span>\n' +
    '<span class="pl-s1">    <span class="pl-s"><span class="pl-pds">\'</span>localsJsonFile<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">true</span></span>\n' +
    '<span class="pl-s1">);</span></pre></div>\n' +
    '<p>Then your locals will be written to json file and path to file will be passed to compiler.</p>\n' +
    '<h2><a href="#pug-cli" aria-hidden="true" class="anchor" id="user-content-pug-cli"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Pug CLI</h2>\n' +
    '<p>Pug also provide a CLI tool:</p>\n' +
    '<div class="highlight highlight-source-shell"><pre>./vendor/bin/pug render-file dir/my-template.pug --output-file </pre></div>\n' +
    '<p>See the <a href="https://www.phug-lang.com/#cli" rel="nofollow">complete CLI documentation here</a></p>\n' +
    '<h2><a href="#check-requirements" aria-hidden="true" class="anchor" id="user-content-check-requirements"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Check requirements</h2>\n' +
    '<p>To check if all requirements are ready to use Pug, use the requirements method:</p>\n' +
    '<div class="highlight highlight-text-html-php"><pre><span class="pl-s1"><span class="pl-smi">$pug</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">Pug</span>(<span class="pl-c1">array</span>(</span>\n' +
    '<span class="pl-s1">    <span class="pl-s"><span class="pl-pds">\'</span>cache<span class="pl-pds">\'</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">\'</span>pathto/writable/cachefolder/<span class="pl-pds">\'</span></span></span>\n' +
    '<span class="pl-s1">);</span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$missingRequirements</span> <span class="pl-k">=</span> <span class="pl-c1">array_keys</span>(<span class="pl-c1">array_filter</span>(<span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>requirements(), <span class="pl-k">function</span> (<span class="pl-smi">$valid</span>) {</span>\n' +
    '<span class="pl-s1">    <span class="pl-k">return</span> <span class="pl-smi">$valid</span> <span class="pl-k">===</span> <span class="pl-c1">false</span>;</span>\n' +
    '<span class="pl-s1">}));</span>\n' +
    '<span class="pl-s1"><span class="pl-smi">$missings</span> <span class="pl-k">=</span> <span class="pl-c1">count</span>(<span class="pl-smi">$missingRequirements</span>);</span>\n' +
    '<span class="pl-s1"><span class="pl-k">if</span> (<span class="pl-smi">$missings</span>) {</span>\n' +
    '<span class="pl-s1">    <span class="pl-c1">echo</span> <span class="pl-smi">$missings</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">\'</span> requirements are missing.&lt;br /&gt;<span class="pl-pds">\'</span></span>;</span>\n' +
    '<span class="pl-s1">    <span class="pl-k">foreach</span> (<span class="pl-smi">$missingRequirements</span> <span class="pl-k">as</span> <span class="pl-smi">$requirement</span>) {</span>\n' +
    '<span class="pl-s1">        <span class="pl-k">switch</span>(<span class="pl-smi">$requirement</span>) {</span>\n' +
    '<span class="pl-s1">            <span class="pl-k">case</span> <span class="pl-s"><span class="pl-pds">\'</span>streamWhiteListed<span class="pl-pds">\'</span></span>:</span>\n' +
    '<span class="pl-s1">                <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">\'</span>Suhosin is enabled and <span class="pl-pds">\'</span></span> <span class="pl-k">.</span> <span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>getOption(<span class="pl-s"><span class="pl-pds">\'</span>stream<span class="pl-pds">\'</span></span>) <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">\'</span> is not in suhosin.executor.include.whitelist, please add it to your php.ini file.&lt;br /&gt;<span class="pl-pds">\'</span></span>;</span>\n' +
    '<span class="pl-s1">                <span class="pl-k">break</span>;</span>\n' +
    '<span class="pl-s1">            <span class="pl-k">case</span> <span class="pl-s"><span class="pl-pds">\'</span>cacheFolderExists<span class="pl-pds">\'</span></span>:</span>\n' +
    '<span class="pl-s1">                <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">\'</span>The cache folder does not exists, please enter in a command line : &lt;code&gt;mkdir -p <span class="pl-pds">\'</span></span> <span class="pl-k">.</span> <span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>getOption(<span class="pl-s"><span class="pl-pds">\'</span>cache<span class="pl-pds">\'</span></span>) <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">\'</span>&lt;/code&gt;.&lt;br /&gt;<span class="pl-pds">\'</span></span>;</span>\n' +
    '<span class="pl-s1">                <span class="pl-k">break</span>;</span>\n' +
    '<span class="pl-s1">            <span class="pl-k">case</span> <span class="pl-s"><span class="pl-pds">\'</span>cacheFolderIsWritable<span class="pl-pds">\'</span></span>:</span>\n' +
    '<span class="pl-s1">                <span class="pl-c1">echo</span> <span class="pl-s"><span class="pl-pds">\'</span>The cache folder is not writable, please enter in a command line : &lt;code&gt;chmod -R +w <span class="pl-pds">\'</span></span> <span class="pl-k">.</span> <span class="pl-smi">$pug</span><span class="pl-k">-&gt;</span>getOption(<span class="pl-s"><span class="pl-pds">\'</span>cache<span class="pl-pds">\'</span></span>) <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">\'</span>&lt;/code&gt;.&lt;br /&gt;<span class="pl-pds">\'</span></span>;</span>\n' +
    '<span class="pl-s1">                <span class="pl-k">break</span>;</span>\n' +
    '<span class="pl-s1">            <span class="pl-k">default</span>:</span>\n' +
    '<span class="pl-s1">                <span class="pl-c1">echo</span> <span class="pl-smi">$requirement</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">\'</span> is false.&lt;br /&gt;<span class="pl-pds">\'</span></span>;</span>\n' +
    '<span class="pl-s1">        }</span>\n' +
    '<span class="pl-s1">    }</span>\n' +
    '<span class="pl-s1">    <span class="pl-k">exit</span>(<span class="pl-c1">1</span>);</span>\n' +
    '<span class="pl-s1">}</span></pre></div>\n' +
    '<h2><a href="#contributing" aria-hidden="true" class="anchor" id="user-content-contributing"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Contributing</h2>\n' +
    '<p>All contributions are welcome, for any bug, issue or merge request (except for secutiry issues) please <a href="/pug-php/pug/blob/master/CONTRIBUTING.md">refer to CONTRIBUTING.md</a></p>\n' +
    '<h2><a href="#security" aria-hidden="true" class="anchor" id="user-content-security"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Security</h2>\n' +
    '<p>Please report any security issue or risk by emailing <a href="mailto:pug@selfbuild.fr">pug@selfbuild.fr</a>. Please don\'t disclose security bugs publicly until they have been handled by us.</p>\n' +
    '<p>Pug-php recommend</p>\n' +
    '</article>',
    url: '//github.com/pug-php/pug',
    imageUrl: '//i.stack.imgur.com/0VpH6.png',
    createdAt: 1519815394000,
  },
  {
    id: 'pug-express-nodejs',
    title: 'Using Pug template in ExpressJS (NodeJS)',
    tags: 'Pug, Express, NodeJS',
    description: 'A template engine enables you to use static template files in your application. At runtime, the template engine replaces variables in a template file with actual values, and transforms the template into an HTML file sent to the client. This approach makes it easier to design an HTML page.',
    content: '<div id="page-doc" markdown="1">\n' +
    '<p>A <em>template engine</em> enables you to use static template files in your application. At runtime, the template engine replaces\n' +
    'variables in a template file with actual values, and transforms the template into an HTML file sent to the client.\n' +
    'This approach makes it easier to design an HTML page.</p>\n' +
    '<p>Some popular template engines that work with Express are <a href="https://pugjs.org/api/getting-started.html">Pug</a>,\n' +
    '<a href="https://www.npmjs.com/package/mustache">Mustache</a>, and <a href="https://www.npmjs.com/package/ejs">EJS</a>.\n' +
    'The <a href="/en/starter/generator.html">Express application generator</a> uses <a href="https://www.npmjs.com/package/jade">Jade</a> as its default, but it also supports several others.</p>\n' +
    '<p>See <a href="https://github.com/strongloop/express/wiki#template-engines">Template Engines (Express wiki)</a>\n' +
    'for a list of template engines you can use with Express.\n' +
    'See also <a href="https://strongloop.com/strongblog/compare-javascript-templates-jade-mustache-dust/">Comparing JavaScript Templating Engines: Jade, Mustache, Dust and More</a>.</p>\n' +
    '<div class="doc-box doc-notice">\n' +
    '<p><strong>Note</strong>: Jade has been renamed to <a href="https://www.npmjs.com/package/pug">Pug</a>. You can continue to use Jade in your app, and it will work just fine. However if you want the latest updates to the template engine, you must replace Jade with Pug in your app.</p>\n' +
    '</div>\n' +
    '<p>To render template files, set the following <a href="/en/4x/api.html#app.set">application setting properties</a>, set in <code>app.js</code> in the default app created by the generator:</p>\n' +
    '<ul>\n' +
    '<li><code>views</code>, the directory where the template files are located. Eg: <code>app.set(\'views\', \'./views\')</code>.\n' +
    'This defaults to the <code>views</code> directory in the application root directory.</li>\n' +
    '<li><code>view engine</code>, the template engine to use. For example, to use the Pug template engine: <code>app.set(\'view engine\', \'pug\')</code>.</li>\n' +
    '</ul>\n' +
    '<p>Then install the corresponding template engine npm package; for example to install Pug:</p>\n' +
    '<pre class="language-sh"><code class="language-sh">$ npm install pug --save\n' +
    '</code></pre>\n' +
    '<div class="doc-box doc-notice">\n' +
    '<p>Express-compliant template engines such as Jade and Pug export a function named <code>__express(filePath, options, callback)</code>,\n' +
    'which is called by the <code>res.render()</code> function to render the template code.</p>\n' +
    '<p>Some template engines do not follow this convention. The <a href="https://www.npmjs.org/package/consolidate">Consolidate.js</a>\n' +
    'library follows this convention by mapping all of the popular Node.js template engines, and therefore works seamlessly within Express.</p>\n' +
    '</div>\n' +
    '<p>After the view engine is set, you don’t have to specify the engine or load the template engine module in your app;\n' +
    'Express loads the module internally, as shown below (for the above example).</p>\n' +
    '<pre class="  language-javascript"><code class="  language-javascript">app<span class="token punctuation">.</span><span class="token keyword">set</span><span class="token punctuation">(</span><span class="token string">\'view engine\'</span><span class="token punctuation">,</span> <span class="token string">\'pug\'</span><span class="token punctuation">)</span>\n' +
    '</code></pre>\n' +
    '<p>Create a Pug template file named <code>index.pug</code> in the <code>views</code> directory, with the following content:</p>\n' +
    '<pre><code class="language-pug">html\n' +
    '  head\n' +
    '    title= title\n' +
    '  body\n' +
    '    h1= message\n' +
    '</code></pre>\n' +
    '<p>Then create a route to render the <code>index.pug</code> file. If the <code>view engine</code> property is not set,\n' +
    'you must specify the extension of the <code>view</code> file. Otherwise, you can omit it.</p>\n' +
    '<pre class="  language-javascript"><code class="  language-javascript">app<span class="token punctuation">.</span><span class="token keyword">get</span><span class="token punctuation">(</span><span class="token string">\'/\'</span><span class="token punctuation">,</span> <span class="token keyword">function</span> <span class="token punctuation">(</span>req<span class="token punctuation">,</span> res<span class="token punctuation">)</span> <span class="token punctuation">{</span>\n' +
    '  res<span class="token punctuation">.</span><span class="token function">render<span class="token punctuation">(</span></span><span class="token string">\'index\'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span> title<span class="token punctuation">:</span> <span class="token string">\'Hey\'</span><span class="token punctuation">,</span> message<span class="token punctuation">:</span> <span class="token string">\'Hello there!\'</span> <span class="token punctuation">}</span><span class="token punctuation">)</span>\n' +
    '<span class="token punctuation">}</span><span class="token punctuation">)</span>\n' +
    '</code></pre>\n' +
    '<p>When you make a request to the home page, the <code>index.pug</code> file will be rendered as HTML.</p>\n' +
    '<p>Note: The view engine cache does not cache the contents of the template’s output, only the underlying template itself. The view is still re-rendered with every request even when the cache is on.</p>\n' +
    '<p>To learn more about how template engines work in Express, see:\n' +
    '<a href="/en/advanced/developing-template-engines.html">“Developing template engines for Express”</a>.</p>\n' +
    '</div>',
    url: '//expressjs.com/en/guide/using-template-engines.html',
    imageUrl: '//rishabh.io/recipes/img/004-express-js.png',
    createdAt: 1519815393000,
  },
];
