import {Routes, RouterModule} from '@angular/router';
import {PageWhatPugComponent} from './pages/page-what-pug/page-what-pug.component';
import {PageHowPugComponent} from './pages/page-how-pug/page-how-pug.component';
import {PageHomeComponent} from './pages/page-home/page-home.component';
import {PageSupportComponent} from './pages/page-support/page-support.component';
import {PageArticleComponent} from './pages/page-article/page-article.component';

const appRoutes: Routes = [
  {
    path: '',
    component: PageHomeComponent
  },
  {
    path: 'what-is-pug-html',
    component: PageWhatPugComponent,
  },
  {
    path: 'support',
    component: PageSupportComponent,
  },
  {
    path: 'how-to-use-pug-html',
    redirectTo: '/blog',
  },
  {
    path: 'blog',
    component: PageHowPugComponent,
  },
  {
    path: 'blog/:id',
    component: PageArticleComponent,
  }
];

export const routing = RouterModule.forRoot(appRoutes);
