import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HtmlRendererComponent} from './components/html-renderer/html-renderer.component';
import {CheckboxComponent} from './components/checkbox/checkbox.component';
import {ClientSideService} from './services/client-side.service';
import {NavBarComponent} from './components/navbar/nav-bar/nav-bar.component';
import {routing} from './app.routing';
import {PageHomeComponent} from './pages/page-home/page-home.component';
import {PageWhatPugComponent} from './pages/page-what-pug/page-what-pug.component';
import {PageHowPugComponent} from './pages/page-how-pug/page-how-pug.component';
import {PageSupportComponent} from './pages/page-support/page-support.component';
import {CardDonateComponent} from './components/card-donate/card-donate.component';
import {TopBannerDonateComponent} from './components/top-banner-donate/top-banner-donate.component';
import {ArticleService} from './services/article.service';
import {CardArticleComponent} from './components/card-article/card-article.component';
import {PageArticleComponent} from './pages/page-article/page-article.component';

@NgModule({
  declarations: [
    AppComponent,
    HtmlRendererComponent,
    CheckboxComponent,
    NavBarComponent,
    PageHomeComponent,
    PageWhatPugComponent,
    PageHowPugComponent,
    PageSupportComponent,
    PageArticleComponent,
    CardDonateComponent,
    CardArticleComponent,
    TopBannerDonateComponent,
  ],
  imports: [
    routing,
    BrowserModule,
    BrowserModule.withServerTransition({appId: 'pughtml'}),
  ],
  providers: [
    ClientSideService,
    ArticleService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
