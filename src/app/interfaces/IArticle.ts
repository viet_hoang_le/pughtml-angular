export interface IArticle {
  id: string; // inner url
  title: string;
  tags: string;
  description: string;
  content: string;
  url: string;
  imageUrl: string;
  createdAt: number;
}
