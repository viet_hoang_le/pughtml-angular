export interface IPugError {
  code?: string;
  msg?: string;
  line?: number;
  column?: number;
}
