import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDonateComponent } from './card-donate.component';

describe('CardDonateComponent', () => {
  let component: CardDonateComponent;
  let fixture: ComponentFixture<CardDonateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardDonateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardDonateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
