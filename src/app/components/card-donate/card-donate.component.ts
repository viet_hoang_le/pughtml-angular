import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-card-donate',
  templateUrl: './card-donate.component.html',
  styleUrls: ['./card-donate.component.scss']
})
export class CardDonateComponent {

 @Output() clickDonate = new EventEmitter();

  clickOnDonate() {
    this.clickDonate.emit();
  }

}
