import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit {

  @Input() label: string;
  @Input() isChecked = false;
  @Output() onToggle = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  click_checkbox = () => this.onToggle.emit(!this.isChecked);

}
