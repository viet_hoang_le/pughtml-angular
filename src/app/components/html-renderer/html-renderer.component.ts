import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-html-renderer',
  templateUrl: './html-renderer.component.html',
  styleUrls: ['./html-renderer.component.scss']
})
export class HtmlRendererComponent {

  @Input() htmlCode = '';

  constructor() { }

}
