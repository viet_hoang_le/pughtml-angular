import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopBannerDonateComponent } from './top-banner-donate.component';

describe('TopBannerDonateComponent', () => {
  let component: TopBannerDonateComponent;
  let fixture: ComponentFixture<TopBannerDonateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopBannerDonateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopBannerDonateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
