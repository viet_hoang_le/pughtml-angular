import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-top-banner-donate',
  templateUrl: './top-banner-donate.component.html',
  styleUrls: ['./top-banner-donate.component.scss']
})
export class TopBannerDonateComponent {

  @Output() closeBanner = new EventEmitter();
  @Output() clickDonate = new EventEmitter();

  clickCloseBanner = () => this.closeBanner.emit();
  clickOnDonate = () => this.clickDonate.emit();

}
