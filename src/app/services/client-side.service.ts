import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser, isPlatformServer} from '@angular/common';
import Editor = AceAjax.Editor;

@Injectable()
export class ClientSideService {

  private _isBrowser = false;
  private _isServer = false;

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
    this._isBrowser = isPlatformBrowser(platformId);
    this._isServer = isPlatformServer(platformId);
  }

  get isBrowser(): boolean {
    return this._isBrowser;
  }

  get isServer(): boolean {
    return this._isServer;
  }

  getWindow(): Window {
    if (!this.isBrowser) {
      return;
    }
    return window;
  }

  scrollTop() {
    if (this.isBrowser) {
      window.scrollTo(0, 0);
    }
  }

  copy2Clipboard(text: string) {
    if (!this.isBrowser) {
      return;
    }
    try {
      const $temp = $('<textarea>');
      $temp.val(text);
      $('body').append($temp);
      $temp.select();

      document.execCommand('copy');
      $temp.remove();
      return true;
    } catch (err) {
      console.log('Oops, unable to copy = ', err);
      return false;
    }
  }

  renderACE(aceId: string, theme: string, onChange: any, text = '', mode = 'ace/mode/html'): Editor {
    if (!this.isBrowser) {
      return;
    }
    const editor = ace.edit(aceId);
    editor.setTheme(theme);
    editor.getSession().setMode(mode);
    editor.getSession().setTabSize(2);
    editor.setValue(text);
    editor.clearSelection();
    editor.on('change', () => onChange(editor.getValue()));
    return editor;
  }

  // resizable divs - requires jquery-ui
  renderResizable(verticalDragElem, leftElem, rightElem) {
    if (!this.isBrowser) {
      return;
    }
    let lastPosition = null;

    const bodyWidth = $('.page-home').width();

    const returnPerCalc = (a, b) => {
      const c = a / b;
      return c * 100;
    };
    const calculatepercent = (position) => {
      const a = position;
      const b = bodyWidth;
      const c = bodyWidth - position;
      $(leftElem).width(returnPerCalc(a, b) + '%');
      $(rightElem).width(returnPerCalc(c, b) + '%');
    };

    $(verticalDragElem).draggable({
      axis: 'x',
      start: function(a: any) {
        calculatepercent(a.target.offsetLeft);
      },
      drag: function(b: any) {
        calculatepercent(b.target.offsetLeft);
      },
      stop: function(c: any) {
        calculatepercent(c.target.offsetLeft);
        lastPosition = c.target.offsetLeft;
      }
    });

    const resizeWindow = () => {
      // Convert the width from px to %
      const percent = $(rightElem).width() / bodyWidth * 100;

      // Get the left postion of drag bar div incase window resized
      const position = (lastPosition != null) ? ((percent * bodyWidth) / 100) : (0.4 * bodyWidth);

      $(verticalDragElem).css({
        'left'	: position
      });
    };

    resizeWindow();
    $( window ).resize(function() {
      resizeWindow()
    });
  }

  gaSendEvent(category: string, action: string, label: string) {
    ga('send', 'event', category, action, label);
  }
  gaEventDonateClick() {
    this.gaSendEvent('donate', 'click_donate', 'Donate button clicked')
  }
  gaEventDonateClose() {
    this.gaSendEvent('donate', 'close_donate', 'Donate top-nav-bar closed')
  }
}
