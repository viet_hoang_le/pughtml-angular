import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {ARTICLES} from '../article-data';
import {map} from 'rxjs/operators';

@Injectable()
export class ArticleService {

  constructor() { }

  getArticles() { return Observable.of(ARTICLES); }

  getArticle(id: string) {
    return this.getArticles().pipe(
      map(articles => articles.find(item => item.id === id))
    );
  }

}
