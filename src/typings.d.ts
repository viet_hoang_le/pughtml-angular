/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

declare var pug: any;

declare var html2jade: any;
declare module 'html2jade' {
  export = html2jade;
}

// google analytics
declare var ga: any;

declare var adsbygoogle: any;
declare interface Window {
  adsbygoogle: any[];
}
