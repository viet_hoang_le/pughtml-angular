# Pughtml

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.3.

## Deploy on Firebase

### Hosting (client side) only
`yarn deploy`

### Server Side Rendering

* Build only: `yarn ssr:b`
* Option 1: Build & run with NodeJS Express: `yarn ssr:bnr`
* Option 2: Build & deploy to Firebase function
    - Build and get `/dist/browser` and `/dist/server`
    - Moving server from `/dist/server` to `/functions/server`
    - Moving index.html from `/dist/browser` to `/functions/server` (remember to not update the assess serve in public: dist/browser
    - Done with: `firebase deploy`
    - **Note that**: public is `dist/browser` specified in `/firebase.json`
    - `/functions/index.js` serve as server.
